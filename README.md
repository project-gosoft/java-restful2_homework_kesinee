# Java Restful2_Homework_Kesinee

## รายละเอียด project มีดังนี้

**สิ่งที่ต้องส่ง** 

   1. นำ code มา run ให้ผ่าน ให้สามารถทดสอบ one to many ได้ด้วย Postman
   2. นำ postman มาแก้ เนื้อหา ให้รองรับการส่ง Sale ไปกับ Postman
   3. ทำระบบ บริหาร ร้านค้า
   4. เพิ่ม ลด แก้ไข Staff, Computer, Shop ได้  
   5. สามารถ ใส่ computer เข้าไปใน shop ได้
   6. สามารถ ลบ computer ออกจาก shop ได้
   7. สามารถ ใส่ Staff เข้าไปใน shop ได้
   8. สามารถ ลบ Staff เข้าไปใน shop ได้
   9. Manager มีความสัมพันธ์กับ Shop แบบ One to One
   10. ใช้ Postman เพื่อทดสอบ ข้อ 1-5 เพื่อดูผลลัพธ์ครบถ้วน


