package com.shop.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.shop.demo.model.Manager;

public interface ManagerRepository extends CrudRepository<Manager, Integer> {

}
